namespace API.Controllers {
    using Microsoft.AspNetCore.Mvc;
    [Route("/")]
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index() {
            return Ok("Welcome");
        }
    }
}