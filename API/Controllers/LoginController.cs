using API.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;


namespace API.Controllers
{
    [ApiController]
    [Route("/login")]
    public class LoginController : ControllerBase {
        private readonly IAuthenticationManager authenticationManager;

        public LoginController(IAuthenticationManager authenticationManager)
        {
            this.authenticationManager = authenticationManager;
        }
        [HttpPost]
        public ActionResult Login([FromBody] UserCredentials userCredentials) {
            var token = authenticationManager.Authenticate(userCredentials.Username, userCredentials.Password);

            if (string.IsNullOrEmpty(token)) return RedirectToAction(nameof(Register));

            return Ok(token);
        }
        [HttpPost("/register")]
        public ActionResult Register([FromBody] UserCredentials userCredentials) {
            authenticationManager.Register(userCredentials.Username, userCredentials.Password);

            var token = authenticationManager.Authenticate(userCredentials.Username, userCredentials.Password);

            return Ok(token);
        }
        // [HttpPost("/logout")]
        // public ActionResult Logout() {
            
        // }
        [HttpGet]
        public ActionResult Get() {
            return Ok("Authorize");
        }
    }
}