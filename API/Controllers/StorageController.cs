using System.Security.Claims;
using System.Text.Json;
using API.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.StaticFiles;

namespace API.Controllers;

[Authorize]
[ApiController]
[Route("/storage")]
public class StorageController : Controller
{
    private string USER_DIRECTORY
    {
        get
        {
            return Directory.GetCurrentDirectory() + "/Users" + $"/{this.User.Identity.Name}";
        }
    }

    private readonly ILogger<StorageController> _logger;

    public StorageController(ILogger<StorageController> logger)
    {
        _logger = logger;
    }
    [HttpGet]
    public ActionResult Get() {
        return Ok(GetShortNames(USER_DIRECTORY));
    }
    [HttpGet("{**path}")]
    public ActionResult Get(string path)
    {
        _logger.LogDebug($"ENTERING GET METHOD");
        _logger.LogTrace($"Current userpath is {USER_DIRECTORY}");
        _logger.LogTrace($"Path is {path}");

        try
        {
            _logger.LogDebug($"Getting entries from {path}");
            return Ok(GetShortNames(path));
        }
        catch (IOException)
        {
            _logger.LogWarning($"{path} is not a directory. Getting file contents");
            string contents;
            try
            {
                using (var sr = new StreamReader(Path.Combine(USER_DIRECTORY, path)))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                _logger.LogWarning($"{path} is not a file");
                return NotFound();
            }

            return Ok(contents);
        }
    }
    [HttpGet("download/{**path}")]
    public FileResult Download(string path) {
        string filePath = Path.Combine(USER_DIRECTORY, path);
        
        var provider = new FileExtensionContentTypeProvider();
        if (!provider.TryGetContentType(filePath, out var contentType))
        {
            contentType = "application/octet-stream";
        }

        var fileByteContent = System.IO.File.ReadAllBytes(filePath);
        return File(fileByteContent, contentType, Path.GetFileName(filePath));
    }
    [HttpPost("{**path}")]
    public ActionResult Create(string path)
    {
        _logger.LogDebug("ENTERING POST METHOD");
        _logger.LogTrace($"Creating folder at {path}");

        string createPath = Path.Combine(USER_DIRECTORY, path);

        _logger.LogTrace($"User path is {USER_DIRECTORY}");
        _logger.LogTrace($"Create path is {createPath}");

        try
        {
            Directory.CreateDirectory(createPath);
        }
        catch (DirectoryNotFoundException)
        {
            return NotFound();
        }
        catch (IOException)
        {
            return BadRequest();
        }
        catch
        {
            return BadRequest();
        }

        _logger.LogTrace($"Directory created at {path}");

        return CreatedAtAction(nameof(Get), path);
    }

    [HttpPost("{**path}")]
    public async Task<ActionResult> CreateFile(string path, IFormFile file)
    {
        using (var fileStream = new FileStream(Path.Combine(USER_DIRECTORY, path, file.FileName), FileMode.Create))
        {
            await file.CopyToAsync(fileStream);
        }

        return Created(Path.Combine(path, file.FileName), "New File");
    }
    [HttpDelete("{**path}")]
    public ActionResult Delete(string path)
    {
        string deletePath = Path.Combine(USER_DIRECTORY, path);
        _logger.LogDebug($"Deleting path is {deletePath}");

        try
        {
            Directory.Delete(deletePath, true);
            _logger.LogInformation($"Folder {deletePath} successfully deleted");
        }
        catch (DirectoryNotFoundException)
        {
            _logger.LogWarning($"{deletePath} is not a directory");
            try
            {
                System.IO.File.Delete(deletePath);
                _logger.LogInformation($"File {deletePath} successfully deleted");
            }
            catch (FileNotFoundException)
            {
                _logger.LogWarning($"{deletePath} is not a file");
                return NotFound();
            }
        }

        return NoContent();
    }
    [HttpPatch("{**path}")]
    public ActionResult Rename(string path, [FromBody]JsonPatchDocument jsonPatch)
    {
        string renamePath = Path.Combine(USER_DIRECTORY, path);
        _logger.LogDebug($"Renaming at {renamePath}");

        try
        {
            DirectoryInfo di = new DirectoryInfo(path);
            jsonPatch.ApplyTo(di);
        }
        catch (DirectoryNotFoundException)
        {
            _logger.LogWarning($"Directory {renamePath} not found");
            try {
                FileInfo fi = new FileInfo(renamePath);
                jsonPatch.ApplyTo(fi);
            }
            catch (FileNotFoundException) {
                _logger.LogWarning($"File {renamePath} not found");
                return NotFound();
            }
        }

        return NoContent();
    }

    private IEnumerable<string> GetShortNames(string partialFolderPath)
    {
        List<string> shortNames = new List<string>();

        try
        {
            foreach (var entry in Directory.EnumerateFileSystemEntries(Path.Combine(USER_DIRECTORY, partialFolderPath)))
            {
                try
                {
                    shortNames.Add(new DirectoryInfo(entry).Name);
                }
                catch
                {
                    shortNames.Add(new FileInfo(entry).Name);
                }
            }
        }
        catch (DirectoryNotFoundException)
        {
            _logger.LogWarning($"Directory {partialFolderPath} not found");
        }
        return shortNames;
    }
}