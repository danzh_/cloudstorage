namespace API.Auth
{
    public interface IAuthenticationManager
    {
        string Authenticate(string username, string password);
        void Register(string username, string password);
    }
}